/*Тренировочная модель по курсу "Архитектура ПО"*/

package ServiceLayer;

import DataMapper.AuthorMapper;
import DataMapper.BookMapper;
import DomainModel.Author;
import DomainModel.Book;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Inga
 */

public class DomainFacade 
{
    
    public static ArrayList<Book> getAllBooks() throws SQLException               // Не статик разумно в случае инициализации ф. в конструкторе (и тогда singleton). 
    {
        BookMapper Instance=BookMapper.getInstance();
        
        return Instance.AllBooksFind();
    };
    
    public static ArrayList<Author> getAllAuthors() throws SQLException
    {
        AuthorMapper Instance=AuthorMapper.getInstance();
        
        return Instance.AllAuthorsFind();
    };
    
}
