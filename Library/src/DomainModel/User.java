/*Тренировочная модель по курсу "Архитектура ПО"*/

package DomainModel;

/**
 *
 * @author Inga
 */

public class User extends Man
{
   
    public enum UserType
    {
        Client,
        Administrator
    };
    
    private UserType Type;
    
    public User(Integer Id, String Surname, String Name, String Patronym, UserType Type) 
    {
        super(Id, Surname, Name, Patronym);
        this.Type=Type;
    };
    
    public UserType getType()
    {
        return this.Type;
    };
    
    public void setType(UserType Type)
    {
        this.Type=Type;
    };
}
