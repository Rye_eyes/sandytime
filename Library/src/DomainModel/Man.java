/*Тренировочная модель по курсу "Архитектура ПО"*/


package DomainModel;

import java.util.ArrayList;

/**
 *
 * @author Inga
 */

public class Man extends DomainObject
{
    private String Surname;
    private String Name;
    private String Patronym;
    
    public Man(Integer Id, String Surname, String Name, String Patronym)
    {
        super(Id);
        this.Name=Name;
        this.Patronym=Patronym;
        this.Surname=Surname;   
    };
    
    public String getSurname()
    {
        return this.Surname;
    };
    
    public void setSurname(String Surname)
    {
        this.Surname=Surname;
    };
    
    public String getName()
    {
        return this.Name;
    };
    
    public void setName(String Name)
    {
        this.Name=Name;
    };
    
    public String getPatronym()
    {
        return this.Patronym;
    };
    
    public void setPatronym(String Patronym)
    {
        this.Patronym=Patronym;
    };
    
}
