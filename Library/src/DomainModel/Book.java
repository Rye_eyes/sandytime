/*Тренировочная модель для проекта по курсу "Архитектура ПО"*/

package DomainModel;

import java.util.ArrayList;

/**
 *
 * @author Inga
 */

public class Book extends DomainObject
{
    private String Title;
    private ArrayList<Author> Authors;
    
    public Book(Integer Id, String Title)
    {
        super(Id);
        this.Title=Title;
        this.Authors=new ArrayList<>();
    };
    
    public String getTitle()
    {
        return this.Title;
    };
    
    public void setTitle(String Title)
    {
        this.Title=Title;
    };
            
    public ArrayList<Author> getAuthors()
    {
        return this.Authors;
    };
    
    public void addToAuthors(Author newAuthor)
    {
        this.Authors.add(newAuthor);
    };
    
    public void removeFromAuthors(Author oldAuthor)
    {
        this.Authors.remove(oldAuthor);
    };
    
}
