/*Тренировочная модель по курсу "Архитектура ПО"*/

package DomainModel;

import java.util.ArrayList;

/**
 *
 * @author Inga
 */

public class Author extends Man
{
    private ArrayList<Book> Books;
    
    public Author(Integer Id, String Surname, String Name, String Patronym)
    {
        super(Id, Surname, Name, Patronym);
        this.Books=new ArrayList<>();
    };
    
    public ArrayList<Book> getBooks()
    {
        return this.Books;
    };
    
    public void addToBooks(Book newBook)
    {
        this.Books.add(newBook);
    };
    
    public void removeFromBooks(Book oldBook)
    {
        this.Books.remove(oldBook);
    };
    
}
