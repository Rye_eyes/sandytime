/*Тренировочная модель для проекта по курсу "Архитектура ПО"*/

package DomainModel;

/**
 *
 * @author Inga
 */

public abstract class DomainObject 
{
    private Integer Id;
    
    public DomainObject(Integer Id)
    {
        this.Id=Id;
    };
    
    public Integer getId()
    {
        return this.Id;
    };
    
    public void setId(Integer Id)
    {
        this.Id=Id;
    };
    
}
