///*Тренировочная модель по курсу "Архитектура ПО"*/
//
//
//package DataMapper;
//
//import static DataMapper.AbstractMapper.AllIdentitiesMap;
//import DomainModel.Author;
//import DomainModel.Book;
//import DomainModel.DomainObject;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.ArrayList;
//
///**
// *
// * @author Inga
// */
//
//public class UserMapper extends AbstractMapper
//{
//    private static UserMapper Instance=null;
//    
//    
//    private AuthorMapper()
//    {
//    };
//    
//    public static AuthorMapper getInstance()
//    {
//        if (Instance==null)
//        {
//            Instance=new AuthorMapper();
//        };
//
//            return Instance;
//    };
//    
//    @Override
//    protected String findByIdStatement()
//    {
//        return "SELECT * FROM AUTHORS WHERE PK=?";
//    };
//    
//    @Override
//    protected String findAllStatement()
//    {
//        return "SELECT * FROM AUTHORS";
//    }
//    
//    public Author AuthorFind(Integer Id) throws SQLException
//    {
//        Author Result=(Author) DomainObjectFind(Id);
//        return Result;
//    };
//    
//    public ArrayList<Author> AllAuthorsFind() throws SQLException
//    {
//        ArrayList<Author> Result=new ArrayList<>();
//       
//        for(DomainObject au: AllDomainObjectFind())
//        {
//            Result.add((Author) au);
//        };
//        
//        return Result;
//    };
//    
//    public ArrayList<Author> FindAuthorsByBookId(Integer Id) throws SQLException
//    {
//
//        ArrayList<Author> Result=new ArrayList<>();
//       
//        for(DomainObject au: FewDomainObjectFind(findAllStatement()+" INNER JOIN Association ON Authors.PK = Association.PK_AUTHORS"
//                + " WHERE Association.PK_BOOKS="+Id.toString()))
//        {
//            Result.add((Author) au);
//        };
//        
//        return Result;
//    };
//    
//
//    @Override
//    protected DomainObject load(ResultSet rs) throws SQLException
//    {
//        Author Result;
//        
//        if (!AllIdentitiesMap.containsKey(rs.getInt("PK")))
//           {
//               Result=new Author(rs.getInt("PK"), rs.getString("Surname"), rs.getString("Name"), rs.getString("Patronym"));
//               
//               // Вызываем загрузчик авторов конкретной книги
//               
//               BookMapper Boo=BookMapper.getInstance();
//               
//               AllIdentitiesMap.put(rs.getInt("PK"), Result);
//               
//               ArrayList<Book> Books=Boo.FindBooksByAuthorId(Result.getId());
//               
//               for (Book a: Books)
//               {
//                   Result.addToBooks(a);
//               };
//           }
//        else
//            Result=(Author) AllIdentitiesMap.get(rs.getInt("PK"));
//        
//        return (DomainObject)Result;
//    };
//    
//    
//    
//}
