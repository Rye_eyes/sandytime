/*Тренировочная модель по курсу "Архитектура ПО"*/


package DataMapper;

import DomainModel.Author;
import DomainModel.Book;
import DomainModel.DomainObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Inga
 */

public class BookMapper extends AbstractMapper
{
    
    private static BookMapper Instance=null;
    
    private BookMapper()
    {
    };
    
    public static BookMapper getInstance()
    {
        if (Instance==null)
        {
            Instance=new BookMapper();
        };

            return Instance;
    };
    
    @Override
    protected String findByIdStatement()
    {
        return "SELECT * FROM BOOKS WHERE PK=?";
    };
    
    @Override
    protected String findAllStatement()
    {
        return "SELECT * FROM BOOKS";
    }
    
    public Book BookFind(Integer Id) throws SQLException
    {
        Book Result=(Book) DomainObjectFind(Id);
        return Result;
    };
    
    public ArrayList<Book> AllBooksFind() throws SQLException
    {
        ArrayList<Book> Result=new ArrayList<>();
       
        for(DomainObject boo: AllDomainObjectFind())
        {
            Result.add((Book) boo);
        };
        
        return Result;
    };
    
    @Override
    protected DomainObject load(ResultSet rs) throws SQLException
    {
        Book Result;
        
        
        if (!AllIdentitiesMap.containsKey(rs.getInt("PK")))
           {
               Result=new Book(rs.getInt("PK"), rs.getString("Title"));
               
               // Регистрируем в IM
               
               AllIdentitiesMap.put(rs.getInt("PK"), Result);
               
               // Вызываем загрузчик авторов конкретной книги
               
               AuthorMapper Au=AuthorMapper.getInstance();
               ArrayList<Author> Authors=Au.FindAuthorsByBookId(Result.getId());
               
               for (Author a: Authors)
               {
                   Result.addToAuthors(a);
               };
               
               // Если поменять местами - рекурсия(!), так - загрузка полного графа
           }
        else
            Result=(Book) AllIdentitiesMap.get(rs.getInt("PK"));
        
        return Result;
    };
    
     public ArrayList<Book> FindBooksByAuthorId(Integer Id) throws SQLException
    {
        ArrayList<Book> Result=new ArrayList<>();
      
       
        for(DomainObject boo: FewDomainObjectFind(findAllStatement()+" INNER JOIN Association ON Books.PK = Association.PK_BOOKS"
                + " WHERE Association.PK_AUTHORS="+Id.toString()))
        {
            Result.add((Book) boo);
        };
        
        return Result;
    };

}
