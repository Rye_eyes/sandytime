/*Тренировочная модель по курсу "Архитектура ПО"*/


package DataMapper;

import DomainModel.Author;
import DomainModel.Book;
import DomainModel.DomainObject;
import DomainModel.Man;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Inga
 */

// Mapper не синглтон, возможны параллельные транзакции с бд
// Но в рамках одной программы - синглтон() 

public abstract class AbstractMapper 
{
    protected String Host = "jdbc:derby://localhost:1527/Library";
    protected String Name = "Inga";
    protected String Pass= "4059042";
    
    protected Connection con;
    protected Statement stmt;
    
    protected static Map<Integer,DomainObject> AllIdentitiesMap=new HashMap();
        
    protected abstract String findByIdStatement();
    protected abstract String findAllStatement();
    
    private void Connect() throws SQLException
    {
        this.con = DriverManager.getConnection(this.Host, this.Name, this.Pass);
        this.stmt = con.createStatement();
    };
    
    @SuppressWarnings("empty-statement")
    private void Disconnect() throws SQLException
    {
        if (this.con!=null)
        {
          this.con.close();
        };
        
        if (this.stmt!=null)
        {
          this.stmt.close();
        };
        
    };
    
    private void ResultSetCleanUp(ResultSet rs) throws SQLException
    {
        rs.close();
    };
    
    private ResultSet FindRow(Integer Id) throws SQLException
    {
        Connect();
        ResultSet rs=stmt.executeQuery(findByIdStatement());
        //Disconnect();
        return rs;
    };
    
    private ResultSet FindRows() throws SQLException
    {
        Connect();
        ResultSet rs=stmt.executeQuery(findAllStatement());
        System.out.println("here");
        //Disconnect();
        return rs;
    };
    
    private ResultSet FindFewRows(String Ask) throws SQLException
    {
        Connect();
        ResultSet rs=stmt.executeQuery(Ask);
        //Disconnect();
        return rs;
    };
    
    @SuppressWarnings("empty-statement")
    public DomainObject DomainObjectFind(Integer Id) throws SQLException
    {
        DomainObject Result=AllIdentitiesMap.get(Id);
        if (Result==null)
        {
            ResultSet rs=FindRow(Id);
            Result=load(rs);
            ResultSetCleanUp(rs);
        };
        
        return Result;
    };
    
    public ArrayList<DomainObject> AllDomainObjectFind() throws SQLException
    {
        ArrayList<DomainObject> Result=new ArrayList<>();
        ResultSet rs=FindRows();
        
        Result=loadMany(rs);
        ResultSetCleanUp(rs);
        
        return Result;
    };
    
    
    public ArrayList<DomainObject> FewDomainObjectFind(String Ask) throws SQLException
    {
        ArrayList<DomainObject> Result=new ArrayList<>();
       
        ResultSet rs=FindFewRows(Ask);
        Result=loadMany(rs);
        ResultSetCleanUp(rs);
        
        return Result;
    };
    
    protected abstract DomainObject load(ResultSet rs) throws SQLException;
    
    protected ArrayList<DomainObject> loadMany(ResultSet rs) throws SQLException
    {
        List<DomainObject> Result=new ArrayList<>();
        
        
        while (rs.next())
        {
            if (load(rs)!=null)
            Result.add(load(rs));
        };
        
        return (ArrayList<DomainObject>) Result;
    };
  
}
