/*Тренировочная модель по курсу "Архитектура ПО"*/


package DataMapper;

import DomainModel.Author;
import DomainModel.Book;
import DomainModel.DomainObject;
import DomainModel.Man;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Inga
 */

public class AuthorMapper extends ManMapper
{
    
    private static AuthorMapper Instance=null;
    
    
    private AuthorMapper()
    {
    };
    
    public static AuthorMapper getInstance()
    {
        if (Instance==null)
        {
            Instance=new AuthorMapper();
        };

            return Instance;
    };
    

    
    public Author AuthorFind(Integer Id) throws SQLException
    {
        Author Result=(Author) DomainObjectFind(Id);
        return Result;
    };
    
    public ArrayList<Author> AllAuthorsFind() throws SQLException
    {
        ArrayList<Author> Result=new ArrayList<>();
       
        for(DomainObject au: AllDomainObjectFind())
        {
            Result.add((Author) au);
        };
        
        return Result;
    };
    
    public ArrayList<Author> FindAuthorsByBookId(Integer Id) throws SQLException
    {

        ArrayList<Author> Result=new ArrayList<>();
       
        for(DomainObject au: FewDomainObjectFind(findAllStatement()+" INNER JOIN Association ON Mansit.PK = Association.PK_Authors"
                + " WHERE Association.PK_BOOKS="+Id.toString()))
        {
            Result.add((Author) au);
        };
        
        return Result;
    };
    

    protected DomainObject load(ResultSet rs) throws SQLException
    {
        if (rs.getInt("Codeclass")!=1) return null;
        
        Man Base;
        Author Result;
        
            
        if (!AllIdentitiesMap.containsKey(rs.getInt("PK")))
           {
               Base=(Man) super.load(rs);
        
               Result=new Author(Base.getId(), Base.getSurname(), Base.getName(), Base.getPatronym());
               
               // Вызываем загрузчик авторов конкретной книги
               
               BookMapper Boo=BookMapper.getInstance();
               
               AllIdentitiesMap.put(rs.getInt("PK"), Result);
               
               ArrayList<Book> Books=Boo.FindBooksByAuthorId(Result.getId());
               
               for (Book a: Books)
               {
                   Result.addToBooks(a);
               };
           }
        else
            Result=(Author) AllIdentitiesMap.get(rs.getInt("PK"));
        
        return (DomainObject)Result;
    };
    
}
