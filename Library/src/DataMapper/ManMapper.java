/*Тренировочная модель по курсу "Архитектура ПО"*/


package DataMapper;

import static DataMapper.AbstractMapper.AllIdentitiesMap;
import DomainModel.Author;
import DomainModel.Book;
import DomainModel.DomainObject;
import DomainModel.Man;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


/**
 *
 * @author Inga
 */

public abstract class ManMapper extends AbstractMapper
{
    
    private Integer Classcode=1;
    
    @Override
    protected String findByIdStatement()
    {
        return "SELECT * FROM MANSIT WHERE PK=?";
    };
    
    @Override
    protected String findAllStatement()
    {
        return "SELECT * FROM MANSIT";
    }
    
        
    public Man ManFind(Integer Id, Integer Classcode) throws SQLException
    {
        this.Classcode=Classcode;
        Man Result=(Man) DomainObjectFind(Id);
        return Result;
    };
    
    public ArrayList<Man> AllManFind(Integer Classcode) throws SQLException
    {
        this.Classcode=Classcode;
        ArrayList<Man> Result=new ArrayList<>();
       
        for(DomainObject au: AllDomainObjectFind())
        {
            Result.add((Man) au);
        };
        
        return Result;
    };
    
    
    @Override
    protected DomainObject load(ResultSet rs) throws SQLException
    {
        Man Result=null;
        
        Result=new Man(rs.getInt("PK"), rs.getString("Surname"), rs.getString("Name"), rs.getString("Patronym"));
        
        return (DomainObject)Result;
    };
    
    
    
}
